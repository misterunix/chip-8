package vm

// RAMSIZE : Total amount of memory
const RAMSIZE = 4096

// GRFSIZE : Sixe of the screen memory
const GRFSIZE = 2048

// VirtualMachine : Main structure of the virtual memory.
type VirtualMachine struct {
	// PC : Program Counter
	PC uint16
	// SP : Stack Pointer
	SP uint16
	// I : Instruction Pointer
	I uint16
	// Registers : Working registers 0x0 - 0xF
	Registers [16]uint8
	// Stack : Stack buffer
	Stack [16]uint16
	// Memory : Main memory aka RAM
	Memory [RAMSIZE]uint8
	// Decode : Dont know.
	Decode string
	//OpCode : Current working Op Code
	OpCode uint16
	// DelayTimer : Delay Timer
	DelayTimer uint8
	// SoundTimer : Sound Timer
	SoundTimer uint8
	// Gfx : Screen Memory
	Gfx [GRFSIZE]uint8
}

// NewVM : Allocate and return a instance of struct VirtualMachine
func NewVM() VirtualMachine {
	vm := VirtualMachine{}
	return vm
}

// Reset : Reset the machine
func (v *VirtualMachine) Reset() {
	for i := 0; i < RAMSIZE; i++ {
		v.Memory[i] = 0
	}
	for i := 0; i < 16; i++ {
		v.Stack[i] = 0
		v.Registers[i] = 0
	}

	v.SP = 0
	v.PC = 0
	v.I = 0

	fontarray := [80]uint8{
		0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
		0x20, 0x60, 0x20, 0x20, 0x70, // 1
		0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
		0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
		0x90, 0x90, 0xF0, 0x10, 0x10, // 4
		0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
		0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
		0xF0, 0x10, 0x20, 0x40, 0x40, // 7
		0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
		0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
		0xF0, 0x90, 0xF0, 0x90, 0x90, // A
		0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
		0xF0, 0x80, 0x80, 0x80, 0xF0, // C
		0xE0, 0x90, 0x90, 0x90, 0xE0, // D
		0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
		0xF0, 0x80, 0xF0, 0x80, 0x80, // F
	}

	for i := 50; i < 130; i++ {
		v.Memory[i] = fontarray[i-50]
	}

}

// Execute : Run one machine cycle
func (v *VirtualMachine) Execute() int {
	var returnCode = 0

	// Checks for overflows
	if v.PC >= RAMSIZE {
		v.PC = 200
	}
	if v.SP >= 16 {
		v.SP = 0
	}

	v.OpCode = uint16(v.Memory[v.PC])<<8 + uint16(v.Memory[v.PC+1])
	v.PC += 2

	switch {
	case v.OpCode&0x1000 == 0x0000: // CLS
		if v.OpCode == 0x00EE {
			for i := 0; i < GRFSIZE; i++ {
				v.Gfx[i] = 0
			}
		}
	case v.OpCode&0x1000 == 0x1000: // JMP addr
		v.PC = v.OpCode & 0x0FFF

	case v.OpCode&0x2000 == 0x2000: // CALL addr
		v.Stack[v.SP] = v.PC
		v.PC = v.OpCode & 0x0FFF

	case v.OpCode&0x3000 == 0x3000: // SE Vx, byte
		x := v.OpCode & 0x0F00 >> 8
		k := v.OpCode & 0x00FF
		if v.Registers[x] == uint8(k) {
			v.PC += 2
		}

	case v.OpCode&0x4000 == 0x4000: // SNE Vx, byte
		x := v.OpCode & 0x0F00 >> 8
		k := v.OpCode & 0x00FF
		if v.Registers[x] != uint8(k) {
			v.PC += 2
		}

	case v.OpCode&0x5000 == 0x5000: // SE Vx, Vy
		x := v.OpCode & 0x0F00 >> 8
		y := v.OpCode & 0x00F0 >> 4
		if v.Registers[x] == v.Registers[y] {
			v.PC += 2
		}

	case v.OpCode&0x6000 == 0x6000: // LD Vx,byte
		x := v.OpCode & 0x0F00 >> 8
		k := v.OpCode & 0x00FF
		v.Registers[x] = uint8(k)

	case v.OpCode&0x7000 == 0x7000: // ADD Vx, byte
		x := v.OpCode & 0x0F00 >> 8
		k := v.OpCode & 0x00FF
		v.Registers[x] += uint8(k)

	case v.OpCode&0x8000 == 0x8000:
		switch {
		case v.OpCode&0x000F == 0x0000: // LD Vx, Vy
			x := v.OpCode & 0x0F00 >> 8
			y := v.OpCode & 0x00F0 >> 4
			v.Registers[x] = v.Registers[y]

		case v.OpCode&0x000F == 0x0001: // OR Vx, Vy
			x := v.OpCode & 0x0F00 >> 8
			y := v.OpCode & 0x00F0 >> 4
			v.Registers[x] = v.Registers[x] | v.Registers[y]

		case v.OpCode&0x000F == 0x0002: // AND Vx, Vy
			x := v.OpCode & 0x0F00 >> 8
			y := v.OpCode & 0x00F0 >> 4
			v.Registers[x] = v.Registers[x] & v.Registers[y]

		case v.OpCode&0x000F == 0x0003: // XOR Vx, Vy
			x := v.OpCode & 0x0F00 >> 8
			y := v.OpCode & 0x00F0 >> 4
			v.Registers[x] = v.Registers[x] ^ v.Registers[y]

		case v.OpCode&0x000F == 0x0004: // ADD Vx, Vy
			x := v.OpCode & 0x0F00 >> 8
			y := v.OpCode & 0x00F0 >> 4
			t := uint16(v.Registers[x] + v.Registers[y])
			if t > 255 {
				v.Registers[0xF] = 1
			} else {
				v.Registers[0xF] = 0
			}
			v.Registers[x] = v.Registers[x] + v.Registers[y]

		case v.OpCode&0x000F == 0x0005: // SUB Vx, Vy
			x := v.OpCode & 0x0F00 >> 8
			y := v.OpCode & 0x00F0 >> 4
			if v.Registers[x] > v.Registers[y] {
				v.Registers[0xf] = 1
			} else {
				v.Registers[0xf] = 0
			}
			v.Registers[x] = v.Registers[x] - v.Registers[y]

		case v.OpCode&0x000F == 0x0006: // SHR Vx {, Vy}
			x := v.OpCode & 0x0F00 >> 8
			//y := v.OpCode & 0x00F0 >> 4
			if v.Registers[x]&0x000F == 1 {
				v.Registers[0xF] = 1
			} else {
				v.Registers[0xF] = 0
			}
			v.Registers[x] = v.Registers[x] >> 1

		case v.OpCode&0x000F == 0x0007: // SUBN Vx, Vy
			x := v.OpCode & 0x0F00 >> 8
			y := v.OpCode & 0x00F0 >> 4
			if v.Registers[y] > v.Registers[x] {
				v.Registers[0xf] = 1
			} else {
				v.Registers[0xF] = 0
			}
			v.Registers[x] = v.Registers[y] - v.Registers[x]

		case v.OpCode&0x000F == 0x000E: // SHL Vx {, Vy}
			x := v.OpCode & 0x0F00 >> 8
			if x&0x80 == 0x80 {
				v.Registers[0Xf] = 1
			} else {
				v.Registers[0xF] = 0
			}
			v.Registers[x] = v.Registers[x] << 1
		}

	case v.OpCode&0x9000 == 0x9000: // SNE Vx, Vy
		x := v.OpCode & 0x0F00 >> 8
		y := v.OpCode & 0x00F0 >> 4
		if v.Registers[x] != v.Registers[y] {
			v.PC += 2
		}

	case v.OpCode&0xA000 == 0xA000: // LD I, addr
		v.I = v.OpCode & 0x0FFF

	case v.OpCode&0xB000 == 0xB000: // JP V0, addr
		v.PC = v.OpCode&0x0FFF + uint16(v.Registers[0x0])

	}
	return returnCode
}
